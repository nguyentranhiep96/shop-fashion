<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class Constant extends Enum
{
    const perPage =   10;
    const orderByAsc = 'ASC';
    const orderByDesc = 'DESC';
}
