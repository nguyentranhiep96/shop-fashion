<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * Class LogLevel
 * @package App\Enums
 */
final class LogLevel extends Enum
{
    const EMERGENCY = 1;
    const ALERT = 2;
    const CRITICAL = 3;
    const ERROR = 4;
    const WARNING = 5;
    const NOTICE = 6;
    const INFO = 7;
    const DEBUG = 8;
}
