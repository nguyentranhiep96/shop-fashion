<?php

namespace App\Helpers;

use Illuminate\Support\Str;

class helpers
{
    public static function customSlug($value)
    {
        return Str::slug($value, '-');
    }
}
