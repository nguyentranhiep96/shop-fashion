<?php

namespace App\Http\Controllers\api;

use App\Enums\LogLevel;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class BaseApiController extends Controller
{
    protected function sendLogError($code, $message, $status, $logLevel = LogLevel::ERROR): JsonResponse
    {
        $context = $this->getContext($code);
        if ($logLevel == LogLevel::EMERGENCY) {
            Log::emergency($message, $context);
        } elseif ($logLevel == LogLevel::ALERT) {
            Log::alert($message, $context);
        } elseif ($logLevel == LogLevel::CRITICAL) {
            Log::critical($message, $context);
        } elseif ($logLevel == LogLevel::ERROR) {
            Log::error($message, $context);
        } elseif ($logLevel == LogLevel::WARNING) {
            Log::warning($message, $context);
        } elseif ($logLevel == LogLevel::NOTICE) {
            Log::notice($message, $context);
        } elseif ($logLevel == LogLevel::INFO) {
            Log::info($message, $context);
        } elseif ($logLevel == LogLevel::DEBUG) {
            Log::debug($message, $context);
        }

        $response = [
            'code' => $code,
            'message' => $message,
        ];

        return response()->json($response, $status);
    }

    protected function sendResponse($message, $status, array $data = null)
    {
        $response = [
            'message' => $message,
            'data' => $data
        ];

        return response()->json($response, $status);
    }

    private function getContext($code = null): array
    {
        if ($code) {
            return $context = [
                'code' => $code,
                'input' => request()->all(),
            ];
        }
        return $context = [
            'input' => request()->all(),
        ];
    }
}
