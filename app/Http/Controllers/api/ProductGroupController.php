<?php

namespace App\Http\Controllers\api;

use App\Enums\Constant;
use App\Enums\Message;
use App\Enums\Status;
use App\Http\Requests\Groups\CreateGroupRequest;
use App\Repositories\modules\groups\GroupRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ProductGroupController extends BaseApiController
{
    private $productGroupRepository;

    public function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->productGroupRepository = $groupRepository;
    }

    public function index(Request $request)
    {
        $optionPaginate = $request->has('option') ? $request->input('option') : Constant::perPage;
        $productGroups = $this->productGroupRepository->paginate(['*'], $optionPaginate, Constant::orderByDesc);
        $totalProGroups = count($this->productGroupRepository->all(['id']));
        return $this->sendResponse(Message::STATUS_OK, Status::OK, [
            'groups' => $productGroups,
            'totalGroups' => $totalProGroups
        ]);
    }

    public function store(CreateGroupRequest $request)
    {
        try {
            $productGroup = $this->productGroupRepository->createGroupProduct($request->all());
            return $this->sendResponse(Message::STATUS_CREATED, Status::CREATED, [
                'group' => $productGroup
            ]);
        } catch (\Exception $e) {
            Log::debug($e);
            return $this->sendLogError(Status::INTERNAL_SERVER_ERROR,
                $e->getMessage(), Status::INTERNAL_SERVER_ERROR);
        }
    }

    public function edit($id)
    {
        try {
            $productGroup = $this->productGroupRepository->findById($id);
            return $this->sendResponse(Message::STATUS_OK, Status::OK, [
                'group' => $productGroup
            ]);
        } catch (\Exception $e) {
            return $this->sendLogError(Status::INTERNAL_SERVER_ERROR,
                $e->getMessage(), Status::INTERNAL_SERVER_ERROR);
        }
    }

    public function update(Request $request, $id)
    {
        try {
            $productGroup = $this->productGroupRepository->createOrUpdateGroupProduct($request->all(), $id);
            return $this->responseStatus(200, 'Update Category Success', $productGroup);
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function destroy($id)
    {
        try {
            $ids = explode(",", $id);
            $this->productGroupRepository->delete($ids);
            return $this->responseStatus(200, 'Xóa thành công');
        } catch (\Exception $e) {
            return $this->responseException(500, $e);
        }
    }

    public function search($query)
    {
        $categories = $this->categoryRepository->searchCategory($query);
        return view('admins.modules.categories.table', compact('categories'));
    }
}
