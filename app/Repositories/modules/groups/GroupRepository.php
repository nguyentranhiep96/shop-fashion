<?php

namespace App\Repositories\modules\groups;

use App\Models\ProductGroup;
use App\Repositories\RepositoryAbstract;
use CustomHelper;

class GroupRepository extends RepositoryAbstract implements GroupRepositoryInterface
{
    public function model()
    {
        return ProductGroup::class;
    }

    public function createGroupProduct(array $group, int $id = null)
    {
        return parent::save([
            'pro_group_name' => $group['pro_group_name'],
            'pro_group_slug' => CustomHelper::customSlug($group['pro_group_name']),
            'pro_group_description' => $group['pro_group_description'],
            'pro_group_status' => $group['pro_group_status'],
            'pro_group_parent_id' => 0,
            'user_practise' => auth()->guard('api')->user()->name
        ], $id);
    }
}
