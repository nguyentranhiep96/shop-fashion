<?php

namespace App\Repositories\modules\groups;

use App\Repositories\RepositoryInterface;

interface GroupRepositoryInterface extends RepositoryInterface
{
    public function createGroupProduct(array $group, int $id = null);
}
