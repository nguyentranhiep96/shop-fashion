import request from "../utils/request";

export function getGroupsApi(optionPage = 5, page = 1) {
    return request({
        url: `groups?option=${optionPage}&&page=${page}`,
        method: 'get',
    })
}

export function createGroupApi(data) {
    return request({
        url: `groups`,
        method: 'post',
        data
    })
}

export function editGroupApi(id) {
    return request({
        url: `groups/`+ id + '/edit',
        method: 'get'
    })
}
