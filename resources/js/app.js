import {extend} from "vee-validate";

require('./bootstrap');

import Vue from 'vue';
import App from './App.vue';
import AllStore from './store/store';
import router from './router';

Vue.router = router;

import './middlewares/auth'

import * as rules from 'vee-validate/dist/rules';
import './validators';

Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

import VueApexCharts from 'vue-apexcharts'

Vue.use(VueApexCharts)
Vue.component('apexchart', VueApexCharts)

Vue.config.productionTip = false;

Vue.component('App', require('./App.vue'));

import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';

Vue.use(VueToast);
let instance = Vue.$toast.open('You did it!');
instance.close();
Vue.$toast.clear();

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store: AllStore,
    template: '<App/>',
    components: {
        App
    }
})
