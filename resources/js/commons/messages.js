const messages = {
    CREATE_GROUP_SUCCESS: 'Tạo danh mục thành công',
    CREATE_GROUP_FAIL: 'Tạo danh mục thất bại!',
    GROUP_PRODUCT_VALID: 'Danh mục đã tồn tại!'
}

export default messages;


