import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '../containers/Full'

import Pos from '../views/Pos/Pos'
import Dashboard from "../views/Dashboard/Dashboard";

// Product
import Products from "../views/Products/Index";
import CreateProduct from "../views/Products/Create";
import CopyProduct from "../views/Products/Copy";
import EditProduct from "../views/Products/Edit";

// Group Product
import GroupProduct from "../views/GroupProducts/index"

// Manufactures
import Manufacture from "../views/Manufactures/index"

// Customers
import Customer from "../views/Customers/index"

// Suppliers
import Supplier from "../views/Suppliers/index"

import Login from "../pages/Login";

Vue.use(Router)

export default new Router({
    mode: 'history', // Demo is living in GitHub.io, so required!
    linkActiveClass: 'open active',
    scrollBehavior: () => ({y: 0}),
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '/pos',
            name: 'Pos',
            component: Pos
        },
        {
            path: '/',
            name: 'Home',
            redirect: '/dashboard',
            component: Full,
            children: [
                {
                    path: '/dashboard',
                    name: 'Dashboard',
                    component: Dashboard
                },
                {
                    path: '/products',
                    name: 'Products',
                    component: Products
                },
                {
                    path: '/products/create',
                    name: 'CreateProduct',
                    component: CreateProduct
                },
                {
                    path: '/products/edit',
                    name: 'EditProduct',
                    component: EditProduct
                },
                {
                    path: '/products/copy',
                    name: 'CopyProduct',
                    component: CopyProduct
                },
                {
                    path: '/groups',
                    name: 'GroupProduct',
                    component: GroupProduct
                },
                {
                    path: '/manufactures',
                    name: 'Manufactures',
                    component: Manufacture
                },
                {
                    path: '/customers',
                    name: 'Customers',
                    component: Customer
                },
                {
                    path: '/suppliers',
                    name: 'Suppliers',
                    component: Supplier
                },
            ]
        },
    ]
})
