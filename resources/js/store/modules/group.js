import { getGroupsApi, createGroupApi, editGroupApi } from '../../api/group.api'

const namespaced = true;
const group = {
    namespaced,
    state: {
        groups: [],
        totalGroups: 0
    },
    getters: {
        groups(state) {
            return state.groups
        },
        totalGroups(state) {
            return state.totalGroups
        }
    },
    mutations: {
        listGroups: (state, groups) => {
            state.groups = groups
        },
        totalGroups: (state, total) => {
            state.totalGroups = total
        }
    },
    actions: {
        getListGroups: ({ commit }, options) => {
            return new Promise((resolve, reject) => {
                getGroupsApi(options.optionPage, options.page).then(response => {
                    commit('listGroups', response.data.groups.data)
                    commit('totalGroups', response.data.totalGroups)
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        addGroup: ({ commit }, group) => {
            return new Promise((resolve, reject) => {
                createGroupApi(group).then(response => {
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        },
        editGroupProduct: ({ commit }, groupId) => {
            return new Promise((resolve, reject) => {
                editGroupApi(groupId).then(response => {
                    resolve(response)
                }).catch(error => {
                    reject(error)
                })
            })
        }
    }
}

export default group
