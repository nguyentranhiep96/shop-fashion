import { extend } from 'vee-validate';

import {
    email,
    required,
    min,
    max,
    confirmed
} from 'vee-validate/dist/rules';

extend('email', {
    ...email,
    message: '{_field_} không đúng định dạng'
});

extend('required', {
    ...required,
    message: 'Vui lòng nhập vào {_field_}'
});

extend('min_name', {
    ...min,
    message: '{_field_} tối thiệu 4 kí tự'
});

extend('max_otp', {
    ...max,
    message: '{_field_} cant biggest than 6 character'
});

extend('min', {
    ...min,
    message: '{_field_} tối thiểu 6 kí tự'
});

extend('max', {
    ...max,
    message: '{_field_} tối đa 50 kí tự'
});

extend('min_email', {
    ...min,
    message: 'The {_field_} cant smaller than 10 character'
});

extend('max_email', {
    ...max,
    message: 'The {_field_} cant biggest than 100 character'
});

extend('confirmed', {
    ...confirmed,
    message: 'The {_field_} no same password'
});
