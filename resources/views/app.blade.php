<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>CMS Shop Fashion</title>
    <link href="{{ asset('/') }}templates/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ asset('/') }}templates/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{ asset('/') }}templates/css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body>
<div id="app"></div>
<script>
    window.Laravel = {
        'csrfToken': '{{ csrf_token() }}'
    }
</script>
<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
